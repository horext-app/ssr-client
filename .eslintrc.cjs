module.exports = {
  root: true,
 parser: "vue-eslint-parser",
  extends: ["@nuxt/eslint-config", "prettier"],
}
